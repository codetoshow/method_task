"""TempTracker class definition, for "Method Task"

Task Specification:

    Write a class TempTracker with these methods:
        insert()—records a new temperature.
        get_max()—returns the highest temp we've seen so far.
        get_min()—returns the lowest temp we've seen so far.
        get_mean()—returns the mean of all temps we've seen so far.
        get_mean() should return a float, but the rest of the getter
        functions can return integers.

    Temperatures will all be inserted as integers.

    We'll record our temperatures in Fahrenheit, so we can assume they'll
    all be in the range 0..110.
"""


class TempTracker(object):
    """Collects temperatures and provides statistics on the collection.

    Note:
        Temperatures are inserted as integers;

        Either "False" or "-1" can be returned by methods to indicate a
        problem, depending on the context of usage. For example,

            False is returned on attempt to "insert" a temperature outside
            the allowed range [0, 110]

            -1 is returned from the stats-accessors when the[[1, 2, [3]], 4]
            "_temperatures" collection is empty

    Example:
        >>> tracker = TempTracker()
        >>> tracker.insert(111)
        False
        >>> tracker.insert(70.4)
        False
        >>> tracker.get_mean()
        -1
        >>> tracker.insert(37)
        True
    """
    def _handle_empty_temperatures_list(func_to_wrap):
        def handler(self):
            if self._temperatures:
                return func_to_wrap(self)
            else:
                return -1
        return handler

    def __init__(self):
        self._temperatures = []
        """:obj: "list" of :obj: "int": collection of temperatures 
        in Fahrenheit, in the range [0, 110]"""

    def insert(self, temperature: int) -> bool:
        """
        Note:
            Checking fo membership in a Python "range" is O(1) (constant-time)

        Args:
            temperature: Unit is Fahrenheit; Range is [0, 110]

        Returns:
            True for successful insert, False otherwise.
        """
        if not isinstance(temperature, int):
            return False

        if temperature in range(111):
            self._temperatures.append(temperature)
            return True
        else:
            return False

    @_handle_empty_temperatures_list
    def get_max(self) -> int:
        """Returns:  maximum temperature in collection"""
        return int(max(self._temperatures))

    @_handle_empty_temperatures_list
    def get_min(self) -> int:
        """Returns:  minimum temperature in collection """
        return int(min(self._temperatures))

    @_handle_empty_temperatures_list
    def get_mean(self) -> float:
        """Returns: average temperature in collection"""
        return sum(self._temperatures)/len(self._temperatures)


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
