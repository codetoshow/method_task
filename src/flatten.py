"""flatten function definition, for "Method Task"

Task Specification:

    Write some code, that will
    flatten an array of arbitrarily nested arrays of integers into a flat array
    of integers. e.g. [[1, 2, [3]], 4] -> [1,2,3,4].


Note:
    The solution here is recursive, good to about 1000 calls/levels
"""


def flatten(nested: list) -> list:
    """Transforms a nested list to a flattened list

    Note:
        An empty list is returned in two conditions:
        - "nested" is an empty list
        - "nested" is not a list

    Example:
        >>> flatten([[1, 2, [3]], 4])
        [1, 2, 3, 4]

        >>> flatten("not a list")
        []

        >>> flatten([])
        []

    Args:
        nested:
    Returns:
        A flattened version of the nested list

    """
    if not isinstance(nested, list):
        return []

    flattened = []
    for item in nested:
        if isinstance(item, list):
            flattened.extend(flatten(item))
        else:
            flattened.append(item)
    return flattened


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)

