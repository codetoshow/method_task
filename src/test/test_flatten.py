"""Tests for the "flatten" function

"""

from tst_subjects import flatten


def test_flatten_flattens_nested_list():
    nested_list = [[1, 2, [], (3,)], "4"]
    # Initial Conditions
    assert(len(nested_list) == 2)
    # Do
    flattened_list = flatten(nested_list)
    # Final Conditions
    assert(len(flattened_list) == 4)


def test_flatten_returns_empty_list_for_non_list_input():
    nested_list = "not a list"
    # Initial Conditions
    assert(not isinstance(nested_list, list))
    # Do
    flattened_list = flatten(nested_list)
    # Final Conditions
    assert(not flattened_list)
    assert(isinstance(flattened_list, list))
