"""Tests for the TempTracker API behavior

"""
from tst_subjects import TempTracker


def test_temp_tracker_insert_returns_true_on_successful_insert():
    # setup
    valid_temperature = 70
    tracker = TempTracker()
    # Initial Conditions
    assert (tracker.get_min() == tracker.get_max() == tracker.get_mean() == -1)
    # Do
    tracker.insert(valid_temperature)
    # Final Conditions
    assert (tracker.get_min() == tracker.get_max() == tracker.get_mean() == 70)


def test_temp_tracker_returns_false_on_non_integer_data_insert():
    # setup
    out_of_range_temperature = "111"
    tracker = TempTracker()
    # Initial Conditions
    assert (tracker.get_min() == tracker.get_max() == tracker.get_mean() == -1)
    # Do
    success = tracker.insert(out_of_range_temperature)
    # Final Conditions
    assert (not success)
    assert (tracker.get_min() == tracker.get_max() == tracker.get_mean() == -1)


def test_temp_tracker_returns_false_on_out_of_range_data_insert():
    # setup
    out_of_range_temperature = 111
    tracker = TempTracker()
    # Initial Conditions
    assert (tracker.get_min() == tracker.get_max() == tracker.get_mean() == -1)
    # Do
    success = tracker.insert(out_of_range_temperature)
    # Final Conditions
    assert (not success)
    assert (tracker.get_min() == tracker.get_max() == tracker.get_mean() == -1)


def test_temp_tracker_returns_negative_one_on_empty_collection_operations():
    """This is indirectly tested in every other test"""
    pass


