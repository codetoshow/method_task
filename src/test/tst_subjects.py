"""
"""

# STANDARD IMPORTS
import os
import sys

def _get_src_root():
    test_dir = os.path.dirname(os.path.abspath(__file__))
    test_dir_parts = test_dir.split(os.path.sep)
    path_parts = test_dir_parts[:-1]
    src_root = os.path.join('/', *path_parts)
    return src_root


source_root = _get_src_root()
sys.path.append(source_root)

__all__ = ["flatten", "TempTracker"]
from flatten import flatten
from temp_tracker import TempTracker



