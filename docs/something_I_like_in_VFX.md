
For years, while working as an Electrical Engineer, my family had no idea
about what I did for a living.  My father in particular would ask me about
it at least once a month.  I'd offer what I thought were clever analogies, but
still faced pained expressions.

In this industry, I can point to a game, commercial or movie and just say, 
"I helped make that"; now I see nothing but smiles!

More seriously, I really appreciate the creation of the VFX-reference-platform,
as well as the adoption of Open software solutions therein.  Its existence
reduces much of the risk and uncertainty in the development and management of 
VFX pipelines.
