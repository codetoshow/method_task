
While at DD, I spent some time on a project to setup a non-DD facility in China. 

Like any multi-site facility, DD has an asset-transfer tool.  We wanted to use this
tool for the "China-Project", so I was tasked with refactoring the tool to 
implement a plugin-framework; this allowed us to use the tools at our end
without modification and help jump-start the China facility by providing as much code
as possible without losing intellectual property; the developers in China 
were responsible for creating plug-in components for our framework code.

Its common practice to compose code using "light-weight" plugin mechanisms.
These mechanisms can often be classified as "white-box" plugin mechanisms.  
The China-Project required "black-box" plugin mechanisms that handled 
discovery, registration, verification (for correct api implementation) and
configuration.

I could be proud of this as it was the first time I created a rigorous
third-party (black-box) "pluggable" application. And I am proud of that
accomplishment.

But I'm most proud of this because it was the cleanest, most readable code I 
had ever written.  We always write code with the intent of making it easily 
readable and understandable for ourselves and others. But the customer for this 
code was the developer who needed to immediately understand 
the code.  This created an ever-present laser-focus on creating simple, readable code.

The experience was an exercise in writing code with maximum empathy for the
reader.  And it has improved the quality of the code I've written since.





