I read a post by Ken Scambler titled *"To Kill A Mockingtest"* in which he
asserts that we should not be using Mocks or Stubs in our tests.
(I later found a more easily digestible presentation by Ken on the same subject.)

I really liked the article because it changed the way I think about design and
testing; Ken challenges many "sacred-cows" around design/test.

Just the idea that we can write tests without using mocks/stubs compelled me
to read the article.  I mean, testing-frameworks include facilities for 
mocks/stubs - how could mocks/stubs not be necessary?

Following the recommendations that Ken lays out in his presentation
promises to make your life with tests much more manageable.

Motivating context for why we need this...
    
    The Three Stages in the Evolution of a TD's Testing Practice.
    1. I don't have time to write tests.
    2. I write tests!  I love having tests! 
    3. I'm constantly fixing the tests.  I hate the tests!
    
    A major promise of having tests is that they provide a safety-net for when 
    you modify your code; refactoring code is safer.
    
    Ideally, you never fix tests; you should only need to update 
    tests when you change functionality of the code.
    
    Too often, in practice, the reality is that changing code breaks tests
    unexpededly.  So we experience arduous test maintenance; its not uncommon to 
    spend more time fixing tests than time on core-code changes.


The thread that frames Ken's discussion is the idea that mocks/stubs should not 
be used.  He asserts that mocks/stubs solve the problem of 
**"how to test poorly designed code"**; he further asserts that when you improve the design 
of your code
- you can write tests without mocks/stubs
- tests are easier to understand and setup
- tests only need to change when there are changes to functionality in the code they test

So that's the thread through the discussion.  The major crux of the discussion 
is that we need to design our code in a more functional way - we need to manage 
the side-effects of our code.

He very clearly illustrates how to do this by redesigning code in 
four distinct scenarios where mocks/stubs are used.

As a final tease to encourage you to read the article, Ken asserts and illustrates
that 

    "Mocks kill TDD"

<< mic-drop >>

Here are the links:
- https://www.rea-group.com/blog/to-kill-a-mockingtest/
- https://www.youtube.com/watch?v=EaxDl5NPuCA&feature=youtu.be



