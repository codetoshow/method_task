# Method-Task  Solution

## Directory structure 
(only shows documents of concern)
```text
├── docs
│   ├── proudest_achievement.md
│   ├── recent_article.md
│   └── something_I_like_in_VFX.md
└── src
    ├── flatten.py
    ├── temp_tracker.py
    └── test
        ├── test_flatten.py
        ├── test_temp_tracker.py
        └── tst_subjects.py
```
## Tests
This repo is setup with Continuous-Integration for automated testing (Tests are
run when changes are pushed to the repo).

## Method-Task Description
Welcome to Method interviewing process!  
This consists of interviews as well as some code questions, below.
The code questions:
In answering the code questions, please submit code as if you intended to 
ship it to production. 
The details matter. 
Tests are expected, as is well written, simple idiomatic code. 

Feel free to use a service like github gist to send us the result or attach in the email body or give as an URL for the files hosted in dropbox or google drive.

Use Python as the language to solve the problems as this is what we mostly 
use in production.

We believe this should take around 4 hours to complete. 
Please let us know (roughly) when we should expect your answers 
(e.g. “over the weekend”). Let us know if you need more time.


It may take us 1 to 3 days to review your answers. 
They’re passed to our pipeline team to review.
    
1. What’s your proudest achievement? It can be a personal project or something you’ve worked on professionally. 
Just a short paragraph is fine, but I’d love to know why you’re proud of it.

2. Tell me about a technical book or article you read recently, 
why you liked it, and why I should read it.

3. Tell me about something on visual effects that you really like, and why.

4. Write some code, that will 
flatten an array of arbitrarily nested arrays of integers into a flat array 
of integers. e.g.[[1, 2, [3]], 4]  -> [1,2,3,4].


5. Write a class TempTracker with these methods:
    insert()—records a new temperature.
    get_max()—returns the highest temp we've seen so far.
    get_min()—returns the lowest temp we've seen so far.
    get_mean()—returns the mean of all temps we've seen so far.
    get_mean() should return a float, 
    but the rest of the getter functions can return integers. Temperatures will all be inserted as integers. We'll record our temperatures in Fahrenheit, so we can assume they'll all be in the range 0..110.

 
 

